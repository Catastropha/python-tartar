import hmac
import hashlib
import base64

from random import choice
from string import digits, ascii_uppercase, ascii_lowercase

import datetime, time, requests, json

class Tartar(object):
	def __init__(self, token, secret):
		self.token = token
		self.secret = secret
		self.upload_url = 'https://tartar.io/upload/'
		self.url_url = 'https://tartar.io/url/'
		self._resize_data = []
		self._resize_delay_data = []
		self._s3_store_data = None
		self._callback_url = ''
		self._lossy = None
		self._error_messages = []

	# Recursive Json Canonicalization
	def _rjc(self, d, s):
		if type(d) == dict:
			d_keys = list(d.keys())
			d_keys.sort()
			for key in d_keys:
				s += str(key)
				s += self._rjc(d[key], '')
			return s
		elif type(d) == list:
			for obj in d:
				s += self._rjc(obj, '')
			return s
		else:
			return str(d)

	# Creates the string and the signature
	def _sign(self, request_method, url, data, user_secret):
		s = ''
		s += request_method.upper()
		s += url
		s = self._rjc(data, s)

		digest = hmac.new(user_secret.encode('utf8'), s.encode('utf8'), digestmod=hashlib.sha256).digest()

		signature = base64.b64encode(digest)

		data['auth']['signature'] = signature.decode('utf8')

		return data

	def resize(self, data):
		self._resize_data += data

	def resize_delay(self, data):
		self._resize_delay_data += data

	def s3_store(self, key, secret, bucket):
		self._s3_store_data = {
			'key': key,
			'secret': secret,
			'bucket': bucket,
		}

	def _sub_validate(self, obj):
		if obj['strategy'] == 'exact':
			if not obj.get('width') and not obj.get('height'):
				self._error_messages.append('exact strategy requires width and height key')
			if self._s3_store_data:
				if not obj.get('path'):
					self._error_messages.append('if you indicated s3_store data then every resize and resize_delay object must have a path key that will show Tartar where to store the file for you')

		elif obj['strategy'] == 'landscape':
			if not obj.get('width'):
				self._error_messages.append('landscape strategy requires width key only')
			if self._s3_store_data:
				if not obj.get('path'):
					self._error_messages.append('if you indicated s3_store data then every resize and resize_delay object must have a path key that will show Tartar where to store the file for you')


		elif obj['strategy'] == 'portrait':
			if not obj.get('height'):
				self._error_messages.append('portrait strategy requires height key only')
			if self._s3_store_data:
				if not obj.get('path'):
					self._error_messages.append('if you indicated s3_store data then every resize and resize_delay object must have a path key that will show Tartar where to store the file for you')


		if obj['strategy'] == 'auto':
			if not obj.get('width') and not obj.get('height'):
				self._error_messages.append('auto strategy requires width and height key')
			if self._s3_store_data:
				if not obj.get('path'):
					self._error_messages.append('if you indicated s3_store data then every resize and resize_delay object must have a path key that will show Tartar where to store the file for you')

		if obj['strategy'] == 'frame':
			if not obj.get('width') and not obj.get('height'):
				self._error_messages.append('frame strategy requires width and height key')
			if self._s3_store_data:
				if not obj.get('path'):
					self._error_messages.append('if you indicated s3_store data then every resize and resize_delay object must have a path key that will show Tartar where to store the file for you')

	def _validate(self):
		for obj in self._resize_data:
			if not obj.get('strategy'):
				self._error_messages.append('resize object is missing strategy key')
			else:
				self._sub_validate(obj)

		for obj in self._resize_delay_data:
			if not obj.get('strategy'):
				self._error_messages.append('resize_delay object is missing strategy key')
			else:
				self._sub_validate(obj)

		if self._resize_delay_data and not self._s3_store_data and not self._callback_url:
			self._error_messages.append('if you have resize_delay objects and do not provide a storing engine then you must provide a callback_url to notify you.')


	def is_valid(self):
		if self._error_messages:
			return False
		else:
			return True

	def _generate_nonce(self):
		return ''.join(choice(ascii_uppercase+ascii_lowercase+digits) for x in range(8))

	def _get_timestamp(self):
		return str(time.mktime(datetime.datetime.now().timetuple()))[:-2]

	def _build_params(self, url=None):
		params = {
			'auth': {
				'token': self.token,
				'nonce': self._generate_nonce(),
				'timestamp': self._get_timestamp(),
			}
		}
		if self._resize_data:
			params['resize'] = self._resize_data

		if self._resize_delay_data:
			params['resize_delay'] = self._resize_delay_data

		if self._s3_store_data:
			params['s3_store'] = self._s3_store_data

		if self._callback_url:
			params['callback_url'] = self._callback_url

		if self._lossy:
			params['lossy'] = self._lossy

		if url:
			params['url'] = url

		return params

	# Direct file upload
	def upload(self, path, callback_url=None, lossy=False):
		self._callback_url = callback_url
		self._lossy = lossy
		self._validate()
		if self.is_valid():
			params = self._build_params()
			params = self._sign('POST', self.upload_url, params, self.secret)
			files = {'file': open(path, 'rb')}
			r = requests.post(self.upload_url, data={'data': json.dumps(params)}, files=files)
			return r.json()

		else:
			return self._error_messages

	# Upload file from url
	def url(self, url, callback_url=None, lossy=False):
		self._callback_url = callback_url
		self._lossy = lossy
		self._validate()
		if self.is_valid():
			params = self._build_params(url)
			params = self._sign('POST', self.url_url, params, self.secret)
			r = requests.post(self.url_url, data={'data': json.dumps(params)})

			return r.json()

		else:
			return self._error_messages
