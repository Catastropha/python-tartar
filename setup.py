from setuptools import setup

with open('README.md') as f:
    readme = f.read()

    setup(name='Tartar',
          version='0.4',
          description='Tartar optimizes your images, accelerates website loadtimes, saves storage and bandwidth',
          long_description=f.read(),
          py_modules=['tartar'],
          )
