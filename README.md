# TARTAR README #

Tartar optimizes images.

Register on https://tartar.io/ and get the API keys.

For more details please visit https://tartar.io/docs/



### Installation ###


```
#!bash

pip install tartar
```


### Initialize ###

```
#!python

from tartar import Tartar

tartar = Tartar(tartar_token)
```


### Amazon S3 store ###
Set this up if you want Tartar to store optimized images on your S3 store for you

```
#!python
tartar.s3_store(aws_key, aws_secret, s3_bucket_name)

```

### Immediate resizing ###


```
#!python

tartar.resize(
  [
    {
    'strategy': 'landscape',
    'width': 600,
    'path': 'media/images/example.jpeg' # you must provide a path to S3 location for every resize object, if tartar.s3_store was set up
    },
    {
    'strategy': 'exact',
    'width': 600,
    'height': 500,
    'path': 'media/images/example.jpeg' # you must provide a path to S3 location for every resize object, if tartar.s3_store was set up
    },
  ]
)
```

### Delayed resizing ###

If you wish to create multiple resizings of an image it's advisable to use tartar.resize_delay()
```
#!python

tartar.resize_delay(
  [
    {
      'strategy': 'portrait',
      'height': 600,
    },
    {
      'strategy': 'frame',
      'width': 600,
      'height': 500,
    },
  ]
)
```
You can also mix, providing tartar.resize for which you'll get the results immediately and tartar.resize_delay which Tartar will process in the background.

### Finally ###

You must feed the file either directly or from url. Both methods make the request to Tartar API with parameters provided using above methods.


```
#!python

tartar.upload(path='/path/to/file')
tartar.url(url='url/to/file')
```
